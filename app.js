const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require("passport");
require("./passport")(passport);

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/authDB",{ useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology:true });

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use("/users", require("./routes/users"));

module.exports = app;