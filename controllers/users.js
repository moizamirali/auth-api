const JWT = require('jsonwebtoken');
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const {JWT_SECRET_KEY, CONFIRM_URL, PASSRESET_URL, issuer, JWT_CONFIRM_KEY} = require('../configuration');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

//create signtoken on user id
signToken = user => {
    return JWT.sign({
        issuer: issuer,
        subject: user.id,
        issueAt: new Date().getTime(),
    }, JWT_SECRET_KEY, { expiresIn: '12h' });
}

//create signtoken on user email
confirmToken = email => {
    return JWT.sign({
        issuer: issuer,
        subject: email,
        issueAt: new Date().getTime(),
    }, JWT_CONFIRM_KEY, { expiresIn: '2h' });
}

//verify and get user id 
verifyToken = token => {
    let jwtToken = token.split(/\s+/)
    var decodedUserToken = JWT.verify(jwtToken[1], JWT_SECRET_KEY)
    return decodedUserToken.subject
}

module.exports = {
    signUp: async (req, res, next) => {
        //extract body vars
        const { fullName, phone, email, password } = req.value.body;
        //connect mailjet api

        //check if user exists
        const foundUser = await User.findOne({ email });
        if (foundUser) {
            // var errors = ['Email already exists'];
            return res.status(403).json({ errors:'Email already exists' })
        }
        var data = {
            fullName: fullName,
            phone: phone,
            email: email,
            confirmed: 1,
            password: password
        }

        //create a new user
        const newUser = new User(data);
        //save user
        await newUser.save();
        // generate token
        const token = signToken(newUser);    
        //assign sign token
        return res.status(200).json({ success:'User created Successfully', id:newUser._id, token: token })
    },

    signIn: async (req, res, next) => {
        const token = signToken(req.user);
        const user = await User.findOne({ email: req.user.email });
        if (user.confirmed == false) {
            return res.status(406).json({ errors: 'user not confirmed' });
        }
        else {
            if (user.status == false) {
                return res.status(406).json({ errors: 'account is disable' });
            }
            if (user.blockStatus === true) {
                return res.status(406).json({ errors: 'user is block' });
            }
            return res.status(200).json({"userID":user.id ,'name': user.fullName, 'email': user.email, token: token, phoneVerified: user.phoneVerified, "picture":user.picture });
        }
    },

    secret: async (req, res, next) => {
        console.log("i am authenticated now");
        var success = [{ "auth": true}];

        return res.status(200).json({ success })
    },

    fetchUsers: async (req, res, next) => {
        try {
            let token = req.headers.authorization
            let currentUserId =  verifyToken(token);
            const users = await User.find({ _id: { $ne: ObjectId(currentUserId) } }).select("-password -__v -updatedAt -confirmed -confirmationCode").exec();
            return res.status(200).json({
                "success": true,
                "data": users
            });
        } catch (err) {
            return res.status(400).json({
                errors: "Something went wrong!"
            });
        }
    },

    fetchUsersById: async (req, res, next) => {
        try {
            const user = await User.findById(req.params.id)
                .select("-password -__v -updatedAt -confirmed -confirmationCode")
                .exec();
            return res.status(200).json({
                "successss": user
            });
        } catch (err) {
            return res.status(400).json({
                errors: "Something went wrong!"
            });
        }
    }

}