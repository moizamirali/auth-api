const joi = require("joi");

module.exports = {
    validateBody: (schema) => {
        return async (req, res, next) => {
            const result = joi.validate(req.body, schema, { abortEarly: false });
            if (result.error) {
                //return res.status(404).json(validations);
                var errors = [];
                for (var i = 0; i < result.error.details.length; i++) {
                    let value = result.error.details[i].message;
                    errors.push(value);
                }
                return res.status(404).json({ errors });
            }

            if (!req.value) {
                req.value = {};
            }
            req.value["body"] = result.value;
            next();
        };
    },
    schemas: {
        // user schema
        signUpSchema: joi.object().keys({
            fullName: joi.string().required().error(errors => { return { message: "Fullname is required" } }),
            phone: joi.string().trim().regex(/^[0-9]{11}$/).required().error(errors => { return { message: "phone is required & max lenght is 11 digits" } }),
            email: joi
                .string()
                .email()
                .required().error(errors => { return { message: "Email is empty or invalid" } }),
            password: joi.string().required().error(errors => { return { message: "Password is required" } }),
            confirmPassword: joi.string().required().valid(joi.ref('password')).error(errors => { return { message: "Password don't match" } }),
        }),
        signInSchema: joi.object().keys({
            email: joi
                .string()
                .email()
                .required().error(errors => { return { message: "Email is empty or invalid" } }),
            password: joi.string().required().error(errors => { return { message: "Password is required" } })
        }),
    }
}