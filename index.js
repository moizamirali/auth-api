const app = require('./app');


// starting the server
const port = process.env.PORT || 3010;

app.listen(port);

console.log('Server is listening on port ' + port);