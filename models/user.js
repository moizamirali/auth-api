const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

//create user schema
const userSchema = new Schema({
    fullName: {
        type: String,
        required: true,
        lowercase: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    confirmed: {
        type: Boolean,
        default: 1
    },
    confirmationCode: {
        type: String,
        default: null
    },

}, {
    timestamps: true
});


userSchema.pre('save', async function (next) {
    try {
        //generate salt
        const salt = await bcrypt.genSalt(10);
        //password hash
        const passwordhash = await bcrypt.hash(this.password, salt);
        //reset pass with hashed one
        this.password = passwordhash;
        next();
    } catch (error) {
        next(error);
    }
});


userSchema.methods.isValidPassword = async function (newPassword) {
    try {
        return await bcrypt.compare(newPassword, this.password);
    } catch (error) {
        throw new Error(error);
    }
}

//create a model 
const User = mongoose.model('user', userSchema);

module.exports = User;