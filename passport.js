const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy = require('passport-local').Strategy;
const { JWT_SECRET_KEY } = require('./configuration');

const User = require('./models/user');

var opt = {};
opt.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opt.secretOrKey = JWT_SECRET_KEY;

module.exports = function (passport) {
    // JSON web token Strategy

    passport.use('jwt-user', new JwtStrategy(opt, async (payload, done) => {
        try {
            // find the user specified in the token
            const user = await User.findById(payload.subject);
            // if user dosen't exist
            if (!user) {
                return done(null, false);
            }
            // return user
            done(null, user);
        } catch (error) {
            // exception handling
            done(error, false);
        }
    }));
    // local Strategy
    passport.use('local-user', new LocalStrategy({
        usernameField: 'email'
    }, async (email, password, done) => {
        try {
            // find the user by email
            const user = await User.findOne({
                email: email
            });
            if (!user) {
                return done(null, false);
            }
            // if not , handling it
            const isMatch = await user.isValidPassword(password);
            if (!isMatch) {
                return done(null, false);
            }
            // return the user
            done(null, user);
        } catch (error) {
            // exception handling
            done(error, false);
        }

    }));

}