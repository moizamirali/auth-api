const express = require('express');
const router = require('express-promise-router')();
const passport = require('passport');
const passportConf = require('../passport');

const { validateBody, schemas } = require("../helpers/routeHelpers");
const UserController = require('../controllers/users.js');

router
  .route("/signup")
  .post(validateBody(schemas.signUpSchema), UserController.signUp);

router
  .route("/signin")
  .post(validateBody(schemas.signInSchema), passport.authenticate('local-user', { session: false }), UserController.signIn);

router
  .route("/secret")
  .get(passport.authenticate('jwt-user', { session: false }), UserController.secret);


router
  .route("/all")
  .get(passport.authenticate('jwt-user', { session: false }), UserController.fetchUsers);

router
  .route("/user/:id")
  .get(passport.authenticate('jwt-user', { session: false }), UserController.fetchUsersById);



module.exports = router;